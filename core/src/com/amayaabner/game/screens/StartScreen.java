package com.amayaabner.game.screens;

import com.amayaabner.game.Ryvan;
import com.amayaabner.game.stages.StartStage;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class StartScreen implements Screen {

    private StartStage stage;
    Ryvan game;

    public StartScreen(final Ryvan game) {
        stage = new StartStage();
        this.game = game;
    }

    @Override
    public void render(float delta) {
        // clear screen
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        // update stage
        stage.draw();
        stage.act(delta);
        // start game if screen touched
        if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}