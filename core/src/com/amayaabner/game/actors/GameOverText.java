package com.amayaabner.game.actors;

import com.amayaabner.game.screens.GameScreen;
import com.amayaabner.game.stages.GameStage;
import com.amayaabner.game.utils.Constants;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by Ryan on 4/13/2017.
 */

public class GameOverText extends Actor {
    private BitmapFont font;
    private int finalScore;
    private int highScore;

    public GameOverText(int score, int highScore) {
        font = new BitmapFont();
        font.setColor(Constants.GAME_OVER_FONT_COLOR);
        font.getData().setScale(Constants.GAME_OVER_TEXT_X_SCALE,Constants.GAME_OVER_TEXT_Y_SCALE);
    }
    @Override
    public void draw(Batch batch, float parentAlpha){
        //String scoreText = "Your score is " + finalScore + ".\nTap to try again!";
        String scoreText = "Current score: " + GameStage.score + "\nHigh score: " + GameStage.getHighScore() + "\nTap to try again!";
        font.draw(batch,scoreText,Constants.APP_WIDTH/2-80,Constants.APP_HEIGHT/4-20);

    }

}
