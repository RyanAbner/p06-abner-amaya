package com.amayaabner.game.stages;

import com.amayaabner.game.actors.Background;
import com.amayaabner.game.utils.Constants;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

import java.util.ArrayList;

public class StartStage extends Stage implements ContactListener {

    // viewport measurements while working with debug renderer
    private static final int VIEWPORT_WIDTH = Constants.APP_WIDTH;
    private static final int VIEWPORT_HEIGHT = Constants.APP_HEIGHT;

    private Texture startTexture;
    private TextureRegion texRegion;
    private TextureRegionDrawable texDrawable;
    private ImageButton startButton;
    private ArrayList<Background> backgroundLayers;

    public StartStage() {
        super(new ScalingViewport(Scaling.stretch, VIEWPORT_WIDTH, VIEWPORT_HEIGHT,
                new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));
        backgroundLayers = new ArrayList<Background>();
        setUpBackground();
        setUpStartButton();
    }

    private void setUpBackground() {
        backgroundLayers.add(new Background(20,10,"sky_layer.png"));
        backgroundLayers.add(new Background(60,30,"cloud_layer.png"));
        backgroundLayers.add(new Background(90,45,"mountain_layer.png"));
        for (Background layer : backgroundLayers){
            addActor(layer);
        }
    }

    private void setUpStartButton() {
        startTexture = new Texture(Constants.START_BUTTON_PATH);
        texRegion = new TextureRegion(startTexture);
        texDrawable = new TextureRegionDrawable(texRegion);
        startButton = new ImageButton(texDrawable);
        startButton.setPosition(getCamera().viewportWidth / 4, getCamera().viewportHeight / 4);
        addActor(startButton);
    }

    @Override
    public void beginContact(Contact contact) {

    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
